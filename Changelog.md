# Upgrade D3.v3 to D3.v4

## 引入方式

v3中是直接全部引入d3模块，而v4中则进行了模块拆分，可以按需引入，直接上代码：

```javascript
// v3
import d3 from 'd3'

// v4
// 部分引入
import {scaleLinear} from "d3-scale"
// 或全部引入
import * as d3 from 'd3'
```

## 比例尺的创建

v4中语法更加语义化，比例尺相关API `d3.scale.xxx()` 均修改为 `d3.scaleXxx()`，如： [d3.scaleLinear()](https://github.com/d3/d3-scale/blob/master/README.md#scaleLinear)

## selection.attr用法

[selection.attr()](https://github.com/d3/d3-selection#selection_attr) 方法不再接受对象参数，即在v4中不再支持批量属性设置，对于v3版本的attr方法使用需要做如下修改：

```javascript
// v3
selection.attr({
    cx: d => d.x,
    cy: d => d.y,
    r: d => colorScale(d.value)
  })

// v4
 selection.attr('cx', d => d.x)
  .attr('cy', d => d.y)
  .attr('r', d => colorScale(d.value))
```

## selection.classed用法

[selection.classed()](https://github.com/d3/d3-selection#selection_classed) 方法不再接受对象参数，使用需要做如下修改：

```javascript
// v3
selection.classed({'foo': true})

// v4
selection.classed('foo', true)
```

## 坐标轴的创建

v4中对坐标轴的创建更加人性化，不在想v3中那么繁琐，创建一个底部坐标轴再定义其 `orient` 为 `bottom`，再创建比例尺 `scale(xxxScale)` 相应代码更新如下：

```javascript
// v3
d3.svg.axis().orient('bottom').scale(xScale)

// v4
d3.axisBottom(xScale)
```

## Category Scales - 序数色值比例尺

```javascript
// v3
d3.scale.category10()

// v4
d3.scaleOrdinal(d3.schemeCategory10)
```

## Dragging - 拖拽

v3中的 `d3.behavior.drag` 重命名为 `d3.drag`, `d3.origin()` 替换成 `d3.subject()`, 相应代码修改如下：

```javascript
// v3
d3.behavior.drag()
  .origin(function (d) { return { x: d[0], y: d[1] } })

// v4
d3.drag()
  .subject(function (d) { return { x: d[0], y: d[1] } })
```

## Shapes - 图形定义

v4中布局的定义中，移除了 `layout` 和 `svg` 关键字，即 `d3.layout.xxx()` 修改为 `d3.xxx()`, `d3.svg.xxx()` 修改为 `d3.xxx()`, 实例代码如下：

```javascript
// v3
var arc = d3.svg.arc()
var pieLayout = d3.layout.pie()

// v4
var arc = d3.arc()
var pieLayout = d3.pie()
```

## Forces - 力导向图

v3版本的力导向图定义 `d3.layout.force` 重名为 `d3.forceSimulation`，v4使用了新的算法，扩展性也更强了，初始化力导向图实例代码如下：

```javascript
// v3
const force = d3.layout.force()
  .charge(-120)
  .linkDistance(30)
  .size([400, 400])

force.nodes(data.nodes)
  .links(data.links)
  .start()

// v4
const force = d3.forceSimulation()
  .nodes(data.nodes)
  .force('center', d3.forceCenter(200, 200))
  .force('link', d3.forceLink(data.links).distance(30))
  .force('charge', d3.forceManyBody().strength(-120))

```

## Geographies - 地理图形

v4中，地理图形相关命名都扁平化了，各种方法中都移除了 `.geo.` 关键字，即 `d3.geo.xxx` 修改为 `d3.geoXxx`, 实例代码如下：

```javascript
// v3
const projection = d3.geo.mercator()
const path = d3.geo.path()

// v4
const projection = d3.geoMercator()
const path = d3.geoPath()
```


## Requests - 请求

v3中的 `d3.xhr` 方法重名为 `d3.request`，同时也整合了v3中的 `d3.csv` 和 `d3.tsv` 加载文本数据文件的方法，不同的文件类型通过 `d3.request(url).mimeType("text/csv")` [mimeType](https://github.com/d3/d3-request#request_mimeType) 方法来区分。
相关实例代码如下：

```javascript
// v3
d3.csv('./dataFromWorldBank.csv',
  d => d,
  (_, rows) => {
    // do something
  }
)

// v4
d3.request('./dataFromWorldBank.csv')
  .mimeType('text/csv')
  .response((xhr) => {
    return d3.csvParse(xhr.responseText)
  })
  .get(function (_, rows) {
    // do something
  })

```