import * as d3 from 'd3'
import './day6.scss'

const data = [{
  time: 8, type: 'Sleep'
}, {
  time: 2, type: 'Exersie'
}, {
  time: 8, type: 'Work'
}, {
  time: 3, type: 'Eat & relax'
}, {
  time: 3, type: 'Study'
}]
const svgArea = document.querySelector('#day6')

// pieLayout.value(d => d * d)
// kepp default value map: d => d
const pieLayout = d3.pie().value(d => d.time)

const color = d3.scaleOrdinal(d3.schemeCategory10)
const arc = d3.arc().outerRadius(130).innerRadius(80)
let tooltip

// 绘制pie图
d3.select(svgArea)
  .classed('day6', true)
  .append('g')
  .attr('class', 'pie')
  .attr('transform', 'translate(200, 200)')
  .selectAll('path')
  .data(pieLayout(data))
  .enter()
  .append('path')
  .attr('d', arc)
  .style('fill', (_, i) => color(i))
  .style('opacity', 0.5)
  .style('stroke', 'black')
  .style('stroke-width', '1px')
  .style('cursor', 'pointer')
  .on('mouseenter', function (d, i) {
    d3.select(this)
      .transition()
      .duration(300)
      .style('opacity', 1)
    d3.select('#day6 .tooltip-type')
      .text(data[i].type)
    d3.select('#day6 .tooltip-time')
      .text(`: ${data[i].time} hours`)
    tooltip
      .style('display', `inline-block`)
    // 联动修改legend
    d3.select('.pie-legend').selectAll('g').style('opacity', function (d, index) {
      return index === i ? 1 : 0.5
    })
  })
  .on('mousemove', () => {
    const point = d3.mouse(svgArea)
    tooltip
      .style('transform', `translate(${point[0] + 5}px, ${point[1] + 5}px)`)
  })
  .on('mouseleave', function (_, i) {
    d3.select(this)
      .transition()
      .duration(300)
      .style('opacity', 0.5)

    tooltip
      .style('display', `none`)

    // 联动修改legend
    d3.select('.pie-legend').selectAll('g').style('opacity', 0.5)
  })

// 绘制中间文本内容
const descrSelection = d3.select(svgArea)
  .append('g')
  .attr('class', 'pie-legend')
  .selectAll('g')
  .data(data)
  .enter()
  .append('g')
  .attr('transform', (_, i) => `translate(160, ${165 + 15 * i})`)
  .style('opacity', 0.5)
descrSelection
  .append('rect')
  .attr('width', 10)
  .attr('height', 10)
  .attr('fill', (_, i) => color(i))

descrSelection
  .append('text')
  .text(d => d.type)
  .attr('width', 10)
  .attr('height', 10)
  .attr('fill', (_, i) => color(i))
  .attr('transform', (_, i) => `translate(15, 10)`)

const newNode = document.querySelector('.day6').parentNode
  .insertBefore(
  document.createElement('div'),
  document.querySelector('.day6')
  )

tooltip = d3.select(newNode)
  .attr('id', 'day6')
  .classed('tooltip', true)

tooltip.append('text')
  .classed('tooltip-type', true)

tooltip.append('text')
  .classed('tooltip-time', true)
