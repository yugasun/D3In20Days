import * as d3 from 'd3'

const currentDay = 5

const data = d3.range(8).map(() => Math.random() * 8 + 2)
const svgArea = document.querySelectorAll('svg')[currentDay - 1]

// pieLayout.value(d => d * d)
// kepp default value map: d => d
const pieLayout = d3.pie()

const color = d3.scaleOrdinal(d3.schemeCategory10)

const arc = d3.arc()
  .outerRadius(100)
  .innerRadius(0)

const pieG = d3.select(svgArea)
  .append('g')
  .attr('transform', 'translate(200, 200)')
  .selectAll('path')
  .data(pieLayout(data))
  .enter()
  .append('g')
  .attr('class', 'pie-arc')
  .style('cursor', 'pointer')
  .on('mouseenter', function (_, i) {
    const g = d3.select(this)
    g.select('path')
      .transition()
      .duration(300)
      .style('opacity', 1)
    g.select('text').style('opacity', 1)
  })
  .on('mouseleave', function (_, i) {
    const g = d3.select(this)
    g.select('path')
      .transition()
      .duration(300)
      .style('opacity', 0.5)
    g.select('text').style('opacity', 0)
  })

pieG.append('path')
  .attr('d', arc)
  .style('fill', (_, i) => color(i))
  .style('opacity', 0.5)

const labelArc = d3.arc().innerRadius(90).outerRadius(50)
pieG.append('text')
  .attr('transform', function (d) {
    let point = labelArc.centroid(d)
    point[0] = point[0] - 10
    return `translate(${point})`
  })
  // .attr('dy', '0.35em')
  .text(function (d) {
    return d.value.toFixed(2)
  })
  .attr('fill', '#fff')
  .attr('stroke', 'none')
  .style('font-size', '12px')
  .style('opacity', 0)
