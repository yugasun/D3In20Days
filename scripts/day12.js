import * as d3 from 'd3'

const data = {
  'nodes': [
    { name: 'Web development', key: 0 },
    { name: 'Front-end', key: 1 },
    { name: 'Back-end', key: 1 },
    { name: 'HTML', key: 2 },
    { name: 'CSS', key: 2 },
    { name: 'Javascript', key: 2 },
    { name: 'Transitions', key: 3 },
    { name: 'Layout', key: 3 },
    { name: 'Frameworks', key: 3 },
    { name: 'ES6', key: 3 }
  ],
  'links': [
    { 'source': 1, 'target': 0 },
    { 'source': 2, 'target': 0 },
    { 'source': 3, 'target': 1 },
    { 'source': 4, 'target': 1 },
    { 'source': 5, 'target': 1 },
    { 'source': 6, 'target': 4 },
    { 'source': 7, 'target': 4 },
    { 'source': 8, 'target': 5 },
    { 'source': 9, 'target': 5 }
  ]
}

const svgArea = document.querySelector('#day12')
const svg = d3.select(svgArea)
const color = d3.scaleOrdinal(d3.schemeCategory10)

const force = d3.forceSimulation()
  .nodes(data.nodes)
  .force('center', d3.forceCenter(200, 200))
  .force('link', d3.forceLink(data.links).distance(80))
  .force('charge', d3.forceManyBody().strength(-120))

const link = svg.selectAll('line')
  .data(data.links)
  .enter()
  .append('line')
  .attr('stroke', '#4A90E2')
  .attr('stroke-opacity', 0.5)
  .attr('stroke-width', 2)

// 初始化 drag事件
const drag = d3.drag()
  .on('start', function () {
    if (!d3.event.active) {
      force.alphaTarget(0.35).restart()
    }
  })
  .on('end', function () {
    force.alphaTarget(0)
    d3.event.subject.fx = null
    d3.event.subject.fy = null
  })
  .on('drag', function () {
    d3.event.subject.fx = d3.event.x
    d3.event.subject.fy = d3.event.y
  })

const node = svg.selectAll('circle')
  .data(data.nodes)
  .enter()
  .append('g')
  .call(drag)

node.append('circle')
  .attr('fill', (d) => {
    return color(d.key)
  })
  .attr('r', 15)

node.append('text')
  .text(d => d.name)
  .attr('font-size', 8)
  .attr('dy', 2)

force.on('tick', function () {
  link
    .attr('x1', d => d.source.x)
    .attr('y1', d => d.source.y)
    .attr('x2', d => d.target.x)
    .attr('y2', d => d.target.y)

  node.attr('transform', d => `translate(${d.x},${d.y})`)
})
