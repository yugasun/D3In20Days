// projection reference https://github.com/mbostock/d3/wiki/Geo-Projections
// W:- E:+ N:+ S:-
import * as d3 from 'd3'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import mapPolygonData from './day14_assets/world.json'
import './day15.scss'

const svgWidth = 400
const svgHeight = 400
const cityData = [
  { name: 'San Francisco', x: -122, y: 37 },
  { name: 'Tokyo', x: 139, y: 35 },
  { name: 'Shanghai', x: 121, y: 31 },
  { name: 'London', x: 0, y: 51 }
  // { name: 'Moscow', x: 37, y: 55 },
  // { name: 'Roma', x: 12, y: 41 },
]

const svgArea = document.querySelector('#day15')
const svg = d3.select(svgArea)

// Map part
function renderMap (isMercator) {
  svg.selectAll('g')
    .remove()
  const projection = (isMercator ? d3.geoMercator() : d3.geoEquirectangular())
    .scale(svgWidth / 6.28)
    .translate([svgWidth / 2, svgHeight / 2])
  const geoPath = d3.geoPath().projection(projection)
  const graticule = d3.geoGraticule()

  svg.append('g')
    .selectAll('path')
    .data(mapPolygonData.features)
    .enter()
    .append('path')
    .attr('class', 'countries')
    .attr('d', geoPath)
    .attr('fill', '#4A90E2')
    .on('mouseout', function () { d3.select(this).attr('fill', '#4A90E2') })
    .on('mouseover', function () { d3.select(this).attr('fill', 'red') })

  svg.insert('path', '.city')
    .datum(graticule)
    .attr('class', 'graticule')
    .attr('d', geoPath)
    .style('fill', 'none')
    .style('stroke', '#777')
    .style('stroke-opacity', 0.5)
    .style('stroke-width', '0.5px')

  const citySelection = svg.append('g')
    .selectAll('g')
    .data(cityData)
    .enter()
    .append('g')
    .attr('class', 'city')
    .attr('transform', d => `translate(${projection([d.x, d.y])[0]}, ${projection([d.x, d.y])[1]})`)

  citySelection.append('circle')
    .attr('r', 2)
    .attr('opacity', 0.5)

  citySelection.append('text')
    .text(d => d.name)
    .attr('font-size', 8)
    .attr('opacity', 0.8)
    .attr('dx', 2)
    .attr('dy', 2)
}

renderMap(true)

// React part
const newNode = document.querySelector('body')
  .insertBefore(
  document.createElement('div'),
  document.querySelector('#day16').parentNode
  )
newNode.id = `day15Input`
class InputWidget extends Component {
  constructor () {
    super()
    this.state = {
      isMercator: true
    }
  }

  onChangeMode () {
    const { isMercator } = this.state
    this.setState({ isMercator: !isMercator })
    renderMap(!isMercator)
  }

  render () {
    const { isMercator } = this.state

    return (
      <div className='input-area'>
        <button
          onClick={this.onChangeMode.bind(this)}
        >
          Change Projection Mode
        </button>
        <h2>
          {`Currently on ${isMercator ? 'mercator' : 'equirectangular'} mode`}
        </h2>
      </div>
    )
  }
}

ReactDOM.render(<InputWidget />,
  document.querySelector(`#day15Input`))
