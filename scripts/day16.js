import * as d3 from 'd3'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import mapPolygonData from './day14_assets/world.json'
import './day16.scss'

const svgWidth = 400
const svgHeight = 400
const cityData = [
  { name: 'San Francisco', x: -122, y: 37 },
  { name: 'Tokyo', x: 139, y: 35 },
  { name: 'Shanghai', x: 121, y: 31 },
  { name: 'London', x: 0, y: 51 }
]

const svgArea = document.querySelector('#day16')
const svg = d3.select(svgArea)
// Map part
svg.selectAll('g')
  .remove()
const projection = d3.geoOrthographic()
  .scale(svgWidth / 6.28 * 10 / 6)
  .translate([svgWidth / 2, svgHeight / 2])
  .center([0, 0])
  .clipAngle(90)

const geoPath = d3.geoPath().projection(projection)
svg.append('g')
  .selectAll('path')
  .data(mapPolygonData.features)
  .enter()
  .append('path')
  .attr('class', 'countries')
  .attr('d', geoPath)
  .attr('fill', '#4A90E2')
  .on('mouseout', function () { d3.select(this).attr('fill', '#4A90E2') })
  .on('mouseover', function () { d3.select(this).attr('fill', 'red') })

const citySelection = svg.append('g')
  .selectAll('g')
  .data(cityData)
  .enter()
  .append('g')
  .attr('transform', d => `translate(${projection([d.x, d.y])[0]}, ${projection([d.x, d.y])[1]})`)
  .attr('class', 'city-selection')
  .style('display', d => {
    return Math.abs(parseInt(d.x)) < 90 ? 'block' : 'none'
  })

citySelection.append('circle')
  .attr('r', 2)
  .attr('opacity', 0.5)

citySelection.append('text')
  .text(d => d.name)
  .attr('font-size', 8)
  .attr('opacity', 0.8)
  .attr('dx', 2)
  .attr('dy', 2)

const onZooming = () => {
  // projection.translate([d3.event.transform.x, d3.event.transform.y]).scale(d3.event.transform.k)
  svg.selectAll('path.countries')
    .attr('d', geoPath)
  svg.selectAll('.city-selection')
    .attr('transform', d => `translate(${projection([d.x, d.y])[0]}, ${projection([d.x, d.y])[1]})`)
  // Hide ploygon in other side of earth
  // the result of projection.rotate() is a three dimensional array
  const currentRotate = projection.rotate()[0]
  svg.selectAll('.city-selection')
    .style('display', d => {
      return Math.abs(parseInt(d.x) + currentRotate) < 90 ? 'block' : 'none'
    })
}

const toClickCity = function (W, H, center, w, h, margin) {
  return d3.zoomIdentity.translate(0, 0).scale(1)
}

const transform = toClickCity(400, 400, {
  x: 400 / 2,
  y: 400 / 2
}, 400, 400, 0)

const mapZoom = d3.zoom()
  .on('zoom', function () {
    svg.selectAll('g').attr('transform', d3.event.transform)
    onZooming()
  })
svg.call(mapZoom)
svg.call(mapZoom.transform, transform)

const rotateScale = d3.scaleLinear()
  .domain([0, svgWidth])
  .range([-180, 180])
svg
  .on('mousedown', function () {
    svg.on('mousemove', function () {
      const point = d3.mouse(this)
      projection.rotate([rotateScale(point[0], 0)])
      onZooming()
    })
  })
  .on('mouseup', () => {
    svg.on('mousemove', null)
  })

// React part
const newNode = document.querySelector('body')
  .insertBefore(
  document.createElement('div'),
  document.querySelector('#day17').parentNode
  )
newNode.id = `day16Input`

const zoomInRatio = 1.5
const zoomOutRatio = 0.75
class InputWidget extends Component {
  onZooming (isZoomIn) {
    projection.translate([
      ((projection.translate()[0] - svgWidth / 2) * (isZoomIn ? zoomInRatio : zoomOutRatio)) + svgWidth / 2,
      ((projection.translate()[0] - svgHeight / 2) * (isZoomIn ? zoomInRatio : zoomOutRatio)) + svgHeight / 2
    ])
      .scale(projection.scale() * (isZoomIn ? zoomInRatio : zoomOutRatio))
    onZooming()
  }

  render () {
    return (
      <div className='input-area'>
        <button onClick={this.onZooming.bind(this, true)}>
          Zoom in
        </button>
        <button onClick={this.onZooming.bind(this, false)}>
          Zoom out
        </button>
      </div>
    )
  }
}

ReactDOM.render(<InputWidget />,
  document.querySelector(`#day16Input`))
