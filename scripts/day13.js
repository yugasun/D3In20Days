// Find the better way to update data tomorrow
import * as d3 from 'd3'
import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'

import './day13.scss'

// d3 part
const data = {
  'nodes': [
  ],
  'links': [
  ]
}
const color = d3.scaleOrdinal(d3.schemeCategory10)
const svgArea = document.querySelector('#day13')
const svg = d3.select(svgArea)
const force = d3.forceSimulation()
  .force('center', d3.forceCenter(200, 200))
  .force('charge', d3.forceManyBody().strength(-340))

const drag = d3.drag()
  .on('start', function (d) {
    if (!d3.event.active) {
      force.alphaTarget(0.35).restart()
    }
  })
  .on('end', function () {
    force.alphaTarget(0)
    d3.event.subject.fx = null
    d3.event.subject.fy = null
  })
  .on('drag', function () {
    d3.event.subject.fx = d3.event.x
    d3.event.subject.fy = d3.event.y
  })

function configForceTick (link, node) {
  force.on('tick', function () {
    link
      .attr('x1', d => d.source.x)
      .attr('y1', d => d.source.y)
      .attr('x2', d => d.target.x)
      .attr('y2', d => d.target.y)

    node.attr('transform', d => {
      return `translate(${d.x},${d.y})`
    })
  })
  force.alpha(1).restart()
}
function updateForce () {
  svg.selectAll('line')
    .remove()
  svg.selectAll('g')
    .remove()

  // 必须在这里初始化 nodes 和 links
  force
    .nodes(data.nodes)
    .force('link', d3.forceLink(data.links).distance(80))

  const link = svg.selectAll('line')
    .data(data.links)
    .enter()
    .append('line')
    .attr('stroke', '#4A90E2')
    .attr('stroke-opacity', 0.5)
    .attr('stroke-width', 2)

  const node = svg.selectAll('g')
    .data(data.nodes)
    .enter()
    .append('g')
    .call(drag)

  node.append('circle')
    .attr('fill', d => color(d.key))
    .attr('r', 15)

  node.append('text')
    .text(d => d.name)
    .attr('font-size', 8)
    .attr('dy', 2)

  configForceTick(link, node)
}

updateForce()
// React part
const newNode = document.querySelector('body')
  .insertBefore(
  document.createElement('div'),
  document.querySelector('#day14').parentNode
  )
newNode.id = `day13Input`

class InputWidget extends Component {
  constructor () {
    super()
    this.state = {
      confirmed: false,
      text: '',
      nodeCounter: 0
    }
  }

  onConfirm () {
    const { text, nodeCounter } = this.state
    this.setState({ confirmed: true, nodeCounter: nodeCounter + 1 })
    data.nodes.push({ name: text, key: 1 })
    updateForce()
  }

  textChange () {
    this.setState({ text: this.refs.textInput.value })
  }

  onAddSibling () {
    const { text, nodeCounter } = this.state
    this.setState({ nodeCounter: nodeCounter + 1 })
    data.nodes.push({ name: text, key: 5 })
    updateForce()
  }

  onAddChild () {
    const { text, nodeCounter } = this.state
    this.setState({ nodeCounter: nodeCounter + 1 })
    data.nodes.push({ name: text, key: 2 })
    data.links.push({ 'source': nodeCounter, 'target': 0 })
    updateForce()
  }

  render () {
    const { confirmed, text } = this.state
    return (
      <div className='input-area'>
        <span className='placeholder' />
        <input
          onChange={this.textChange.bind(this)}
          ref='textInput'
          type='text'
        />
        <button
          onClick={this.onConfirm.bind(this)}
          disabled={confirmed || text === ''}
        >
          Confirm
        </button>
        <button
          onClick={this.onAddSibling.bind(this)}
          disabled={!confirmed || text === ''}
        >
          Add sibling
        </button>
        <button
          onClick={this.onAddChild.bind(this)}
          disabled={!confirmed || text === ''}
        >
          Add child
        </button>
      </div>
    )
  }
}

InputWidget.propTypes = {
  confirmed: PropTypes.bool
}

ReactDOM.render(<InputWidget />, document.querySelector(`#day13Input`))
