import * as d3 from 'd3'

const svgArea = document.querySelector('#day8')
const data = {
  name: 'root',
  children: [
    {
      name: 'leaf',
      children: [
        { name: 'leaf' },
        { name: 'leaf' }
      ]
    }, {
      name: 'leaf',
      children: [
        { name: 'leaf' },
        { name: 'leaf' }
      ]
    }, {
      name: 'leaf'
    }, {
      name: 'leaf'
    }
  ]
}

const tree = d3.tree()
  .size([350, 350])
const root = d3.hierarchy(data)

// const linkGenerator = d3.diagonal()
const depthScale = d3.scaleOrdinal(d3.schemeCategory10)
const nodeContainers = d3.select(svgArea)
  .append('g')
  .attr('transform', 'translate(30, 30)')

// 绘制连线
nodeContainers.selectAll('path')
  .data(tree(root).links())
  .enter()
  .append('path')
  .attr('d', function (d) {
    return `M ${d.source.y} ${d.source.x}
            C ${(d.source.y + d.target.y) / 2} ${d.target.x},
              ${(d.source.y + d.target.y) / 2} ${d.target.x},
              ${d.target.y} ${d.target.x}`
  })
  .style('fill', 'none')
  .style('stroke', 'black')
  .style('stroke-width', '1')

// 绘制节点和节点名称
const nodeG = nodeContainers.selectAll('g')
  .data(tree(root).descendants())
  .enter()
  .append('g')
  .attr('transform', d => `translate(${d.y}, ${d.x})`)

nodeG.append('circle')
  .attr('fill', d => depthScale(d.depth))
  .attr('r', 10)
  .attr('stroke', 'white')
  .attr('stroke-width', 2)

nodeG.append('text')
  .text(d => d.data.name)
  .attr('transform', 'translate(-15, -10)')
