import * as d3 from 'd3'

const data = {
  name: 'root',
  children: [
    { name: 'leaf1' },
    { name: 'leaf2' },
    { name: 'leaf3' },
    { name: 'leaf4' }
  ]
}
const svgArea = document.querySelector('#day7')
const color = d3.scaleOrdinal(d3.schemeCategory10)

const root = d3.hierarchy(data)
const tree = d3.tree()
  .size([150, 150])

const svgGroup = d3.select(svgArea).append('g')
  .attr('transform', 'translate(' + 120 + ',' + 120 + ')')

const node = svgGroup
  .selectAll('circle')
  .data(tree(root).descendants())
  .enter()

node
  .append('circle')
  .attr('cx', d => d.x)
  .attr('cy', d => d.y)
  .attr('r', 9)
  .attr('fill', (d, i) => color(i))

svgGroup
  .selectAll('line')
  .data(tree(root).links())
  .enter()
  .append('line')
  .attr('x1', d => d.source.x)
  .attr('x2', d => d.target.x)
  .attr('y1', d => d.source.y)
  .attr('y2', d => d.target.y)
  .attr('stroke', 'black')
  .attr('stroke-width', '1px')

node.append('text')
  .text(function (d) {
    return d.data.name
  })
  .attr('x', d => d.x - 20)
  .attr('y', d => d.y - 10)
  .attr('fill', '#22A6F5')
