import * as d3 from 'd3'

const data = {
  'nodes': [
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  'links': [
    { 'source': 1, 'target': 0 },
    { 'source': 2, 'target': 0 },
    { 'source': 3, 'target': 0 },
    { 'source': 4, 'target': 1 },
    { 'source': 5, 'target': 2 },
    { 'source': 5, 'target': 2 }
  ]
}

const svgArea = document.querySelector('#day9')
const svg = d3.select(svgArea)
const color = d3.scaleOrdinal(d3.schemeCategory10)

// 初始化 forceSimulation
const force = d3.forceSimulation()
  .nodes(data.nodes)
  .force('center', d3.forceCenter(200, 200))
  .force('link', d3.forceLink(data.links).distance(100))
  .force('charge', d3.forceManyBody().strength(-120))

// 绘制连线
const link = svg.selectAll('line')
  .data(data.links)
  .enter()
  .append('line')
  .attr('stroke', '#999')
  .attr('stroke-opacity', 0.5)
  .attr('stroke-width', 3)

// 初始化 drag事件
const drag = d3.drag()
  .on('start', function () {
    if (!d3.event.active) {
      force.alphaTarget(0.35).restart()
    }
  })
  .on('end', function () {
    force.alphaTarget(0)
    d3.event.subject.fx = null
    d3.event.subject.fy = null
  })
  .on('drag', function () {
    d3.event.subject.fx = d3.event.x
    d3.event.subject.fy = d3.event.y
  })

// 绘制节点
const node = svg.selectAll('circle')
  .data(data.nodes)
  .enter()
  .append('circle')
  .attr('fill', (d, i) => color(i))
  .attr('r', 10)
  .call(drag)

// 初始化各节点力的作用
force.on('tick', function () {
  link.attr('x1', function (d) { return d.source.x })
    .attr('y1', function (d) { return d.source.y })
    .attr('x2', function (d) { return d.target.x })
    .attr('y2', function (d) { return d.target.y })

  node.attr('cx', function (d) { return d.x })
    .attr('cy', function (d) { return d.y })
})
