import * as d3 from 'd3'

const randomPos = maximum => Number.parseInt(Math.random() * maximum)

// showcase width 400px same as height
const data = d3.range(20).map(() => [randomPos(400), randomPos(400)])

const color = d3.scaleOrdinal(d3.schemeCategory10)

const svgArea = document.querySelector('#day4')

/**
 * 必须先定义 drag对象，
 * 然后再使用selection.call() 调用
 */
const drag = d3.drag()
  .subject(function (d) { return { x: d[0], y: d[1] } })
  .on('drag', function (d) {
    d[0] = d3.event.x
    d[1] = d3.event.y
    d3.select(this)
      .attr('transform', `translate (${d})`)
  })

d3.select(svgArea).selectAll('circle')
  .data(data)
  .enter()
  .append('circle')
  .attr('transform', d => `translate (${d})`)
  .attr('r', 20)
  .attr('fill', (_, i) => color(i))
  .on('mouseenter', function (d) {
    d3.select(this)
    .attr('opacity', 0.8)
  })
  .on('mouseleave', function (d) {
    d3.select(this)
    .attr('opacity', 1)
  })
  .call(drag)

// not work => drag(d3.select(svgArea).selectAll('circle'))
