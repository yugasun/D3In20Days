// thanks to https://github.com/codeforamerica/click_that_hood
import * as d3 from 'd3'

import data from './day20_assets/world.geojson'
// import './day20.scss'

const svgArea = document.querySelector('#day20')
const svg = d3.select(svgArea)

const projection = d3.geoConicConformal()
  .scale(200 / Math.PI)
  .translate([200, 200])

const geoPath = d3.geoPath().projection(projection)

svg.append('g')
    .append('path')
    .attr('d', geoPath(data))
    .attr('fill', 'red')
