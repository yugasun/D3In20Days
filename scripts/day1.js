import * as d3 from 'd3'

const data = [
  {
    value: 0,
    x: 50,
    y: 13
  }, {
    value: 3,
    x: 150,
    y: 63
  }, {
    value: 5,
    x: 250,
    y: 93
  }, {
    value: 7,
    x: 100,
    y: 313
  }, {
    value: 9,
    x: 150,
    y: 213
  }
]

const svgArea = document.querySelector('#day1')

const valueRange = d3.extent(data, d => d.value)
const circleScale = d3.scaleLinear().domain(valueRange).range([5, 60])
const color = d3.scaleOrdinal(d3.schemeCategory10)

// add xAxis
// const xRange = d3.extent(data, d => d.x)
// const xAxisScale = d3.scaleLinear().domain([0, xRange[1]]).range([0, 300])
// const xAxis = d3.axisBottom(xAxisScale)
// d3.select(svgArea).append('g')
//   .attr('transform', 'translate(5, 360)')
//   .call(xAxis)

d3.select(svgArea).selectAll('rect')
  .data(data)
  .enter()
  .append('circle')
  .attr('cx', d => d.x)
  .attr('cy', d => d.y)
  .attr('r', d => circleScale(d.value))
  .attr('fill', (d, i) => color(i))
  .attr('opacity', 0.5)
