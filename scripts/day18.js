import * as d3 from 'd3'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import './day18.scss'

let data = []
let newNode
let tooltip

d3.request('./dataFromWorldBank.csv')
  .mimeType('text/csv')
  .response((xhr) => {
    return d3.csvParse(xhr.responseText)
  })
  .get(function (_, rows) {
    const filteredRows = rows.filter(d => {
      let counter = 0
      for (let i = 0; i < 54; i++) {
        if (d[`${1960 + i}`] === '') {
          counter++
        }
      }
      return counter < 10
    })
    filteredRows.forEach(d => {
      let obj = {}
      obj.name = d['Indicator Name']
      obj.values = []
      for (let i = 0; i < 54; i++) {
        if (d[`${1960 + i}`] !== '') {
          obj.values.push({ value: +d[`${1960 + i}`], year: i })
        }
      }
      data.push(obj)
    })
    ReactDOM.render(<Controller />, newNode)
  })

const svgArea = document.querySelector('#day18')
svgArea.classList.add(`day18`)
const svg = d3.select(svgArea)

function updateChart (values) {
  svg.select('path')
    .remove()
  svg.selectAll('circle')
    .remove()
  const yScale = d3.scaleLinear()
    .domain(d3.extent(values, d => d.value)).range([0, 390])

  const lineGenerator = d3.line()
    .x(d => d.year * 7)
    .y(d => 400 - yScale(d.value))
    .curve(d3.curveCatmullRom)
  // .interpolate('linear')
  // .interpolate('basis')
  svg.append('path')
    .datum(values)
    .attr('d', lineGenerator)
    .attr('stroke', '#4A90E2')
    .attr('fill', 'none')
  svg.selectAll('circle')
    .data(values)
    .enter()
    .append('circle')
    .attr('r', 3)
    .attr('stroke', '#4A90ff')
    .attr('fill', '#4A90ff')
    .attr('cx', d => d.year * 7)
    .attr('cy', d => 400 - yScale(d.value))
    .attr('display', (d, i) => i % 3 === 2 ? 'block' : 'none')
    .on('mouseenter', d => {
      tooltip.style.display = 'inline-block'
      const point = d3.mouse(svgArea)
      tooltip
        .style.transform = `translate(${point[0] + 5}px, ${point[1] + 5}px)`
      tooltip.innerHTML = `${d.year + 1960}, ${d.value}`
    })
    .on('mouseleave', () => {
      tooltip.style.display = 'none'
    })
}
// React part
newNode = document.querySelector('body')
  .insertBefore(
  document.createElement('div'),
  document.querySelector('#day19').parentNode
  )
newNode.id = `day18Input`

tooltip = document.querySelector(`.day18`).parentNode
  .insertBefore(
  document.createElement('div'),
  document.querySelector(`.day18`)
  )
tooltip.id = `day18Tooltip`
tooltip.innerHTML = 'dawdwdw'

class Controller extends Component {
  constructor () {
    super()
    this.state = { index: 0 }
  }

  componentDidMount () {
    updateChart(data[0].values)
  }

  onNavigate (isNext) {
    const { index } = this.state

    let newIndex = index + (isNext ? 1 : -1)
    newIndex = newIndex < 0 ? data.length - 1 : newIndex
    newIndex = newIndex > data.length - 1 ? 0 : newIndex
    this.setState({ index: newIndex })
    updateChart(data[newIndex].values)
  }

  render () {
    const { index } = this.state
    return (
      <div>
        <h1>{data[index].name}</h1>
        <button onClick={this.onNavigate.bind(this, false)}>{'<'}</button>
        <button onClick={this.onNavigate.bind(this, true)}>{'>'}</button>
      </div>
    )
  }
}
