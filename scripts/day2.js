// day 2 target: normal barchart with axis
import * as d3 from 'd3'

const barCount = 10
const data = []
for (let i = 0; i < barCount; i++) {
  data.push(Number.parseInt(Math.random() * 100))
}

const xScale = d3.scaleLinear().domain([0, barCount]).range([0, 360])
const yScale = d3.scaleLinear().domain([0, 100]).range([0, 360])

const xLeftPadding = 32
const yTopPadding = 15

const barGap = 5
const barWidth = 360 / barCount - barGap

const svgArea = document.querySelector('#day2')
d3.select(svgArea).selectAll('rect')
  .data(data)
  .enter()
  .append('rect')
  .attr('height', d => yScale(d))
  .attr('width', barWidth)
  .attr('x', (_, i) => xScale(i) + xLeftPadding)
  .attr('y', d => 360 - yScale(d) + yTopPadding)
  .attr('fill', '#4A90E2')
  .on('mouseover', function () {
    d3.select(this)
      .attr('fill', '#03336B')
  })
  .on('mouseout', function () {
    d3.select(this)
      .attr('fill', '#4A90E2')
  })

const xAxis = d3.axisBottom(xScale)

d3.select(svgArea)
  .append('g')
  .call(xAxis)
  .attr('id', 'xAxis')
  .attr('transform', `translate(${xLeftPadding}, ${360 + yTopPadding})`)
  .attr('fill', 'none')
  .attr('stroke', 'black')

// 这里因为svg中 [0,0] 点是从左上角开始的
const yAxisScale = d3.scaleLinear().domain([0, 100]).range([360, 0])
const yAxis = d3.axisLeft(yAxisScale)

d3.select(svgArea)
  .append('g')
  .call(yAxis)
  .attr('id', 'yAxis')
  .attr('transform', `translate(${xLeftPadding}, ${yTopPadding})`)
  .attr('fill', 'none')
  .attr('stroke', 'black')
