// https://bost.ocks.org/mike/map/
import * as d3 from 'd3'
import topojson from 'topojson'

import data from './day10_assets/cn.json'

const svgArea = document.querySelector('#day10')
const svg = d3.select(svgArea)
// datum and data?
// datum bind data, but not calculate data join, so without enter & exit stuff

const subunits = topojson.feature(data, data.objects.subunits)
const projection = d3.geoMercator().translate([-290, 350]).scale(280)
const graticule = d3.geoGraticule()
const path = d3.geoPath().projection(projection).pointRadius(1)

// 绘制地理区域
svg.selectAll('.subunit')
  .data(subunits.features)
  .enter().append('path')
  .attr('fill', '#D0021B')
  .attr('d', path)

// 绘制grid线
svg.insert('path', '.place')
  .datum(graticule)
  .attr('class', 'graticule')
  .attr('d', path)
  .style('fill', 'none')
  .style('stroke', '#777')
  .style('stroke-opacity', 0.5)
  .style('stroke-width', '0.5px')

// 绘制地区圆点
svg.append('path')
  .datum(topojson.feature(data, data.objects.places))
  .attr('d', path)
  .attr('opacity', '0.6')
  .attr('class', 'place')

// 绘制地图标签
svg.selectAll('.place-label')
  .data(topojson.feature(data, data.objects.places).features)
  .enter().append('text')
  .attr('transform', d => `translate(${projection(d.geometry.coordinates)})`)
  .attr('dy', '.35em')
  .attr('dx', '1px')
  .style('font-size', '3.5px')
  .style('opacity', '0.6')
  .text(function (d) { return d.properties.name })
