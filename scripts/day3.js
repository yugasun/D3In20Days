// day 3 target: transitions 101
import * as d3 from 'd3'

const svgArea = document.querySelector('#day3')
d3.select(svgArea)
  .append('rect')
  .attr('width', 100)
  .attr('height', 30)
  .attr('x', 100)
  .attr('y', 100)

const transition = () => {
  d3.select('#day3 rect')
  .transition()
  .duration(1 * 1000)
  .attr('fill', '#4A90E2')
  .transition()
  .duration(1 * 1000)
  .attr('width', 400)
  .transition()
  .duration(1 * 1000)
  .attr('fill', '#000')
  .transition()
  .duration(1 * 1000)
  .attr('height', 100)
  .transition()
  .duration(1 * 1000)
  .attr('width', 100)
  .transition()
  .duration(1 * 1000)
  .attr('height', 30)
}

transition()
setInterval(() => {
  transition()
}, 6 * 1000)
