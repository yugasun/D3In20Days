import * as d3 from 'd3'
import topojson from 'topojson'

// For generate polygon GEO data, demo:
// ogr2ogr \
//   -f GeoJSON \
//   -where "ADM0_A3 IN ('CHN')" \
//   subunits.json \
//   ne_10m_admin_0_map_subunits.shp

// For generate places GEO data, demo:
// ogr2ogr \
//   -f GeoJSON \
//   -where "ISO_A2 = 'CN' AND SCALERANK < 8" \
//   places.json \
//   ne_10m_populated_places.shp

// For generate topojson data, demo:
// topojson \
//   -o cn.json \
//   --id-property SU_A3 \
//   --properties name=NAME \
//   -- \
//   subunits.json \
//   places.json

import data from './day11_assets/us.json'

const svgArea = document.querySelector('#day11')
const svg = d3.select(svgArea)

const subunits = topojson.feature(data, data.objects.subunits)
const projection = d3.geoMercator().translate([620, 400]).scale(200)

const path = d3.geoPath().projection(projection).pointRadius(1)

// 绘制地图区域
svg.selectAll('.subunit')
  .data(subunits.features)
  .enter().append('path')
  .attr('fill', '#4A90E2')
  .attr('d', path)

// 绘制地图标注圆点
svg.append('path')
  .datum(topojson.feature(data, data.objects.places))
  .attr('d', path)
  .attr('opacity', '0.6')
  .attr('class', 'place')

// 绘制地图标注文字
svg.selectAll('.place-label')
  .data(topojson.feature(data, data.objects.places).features)
  .enter().append('text')
  .attr('transform', d => `translate(${projection(d.geometry.coordinates)})`)
  .attr('dy', '.35em')
  .attr('dx', '1px')
  .style('font-size', '3.5px')
  .style('opacity', '0.6')
  .text(function (d) { return d.properties.name })
