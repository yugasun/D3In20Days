// data from http://emeeks.github.io/d3ia/world.geojson
// curl xxxx -o newFileName
import * as d3 from 'd3'

import data from './day14_assets/world.json'
import './day14.scss'

const svgWidth = 400
const svgHeight = 400

const svgArea = document.querySelector('#day14')
const svg = d3.select(svgArea)

const projection = d3.geoMercator()
  .scale(svgWidth / 6.28)
  .translate([svgWidth / 2, svgHeight / 2])
const path = d3.geoPath().projection(projection)

svg.selectAll('path')
  .data(data.features)
  .enter()
  .append('path')
  .attr('class', 'countries')
  .attr('d', path)
  .attr('fill', 'red')
