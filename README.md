# D3速成20天

> 本项目基于 [D3In20Days](https://github.com/EcutDavid/D3In20Days) 项目修改而来，原项目主要基于d3的3.0版本，本项目升级到了4.0，并对相应的模块进行了升级和优化，升级日志可以查看 [ChangeLog.md](https://github.com/yugasun/D3In20Days/blob/master/Changelog.md)，同时对相关代码添加了中文注释，方便英文不好的朋友阅读~

[LiveDemo](https://yugasun.github.io/D3In20Days/)

## 本地开发

```shell
npm i
npm run dev
```

可以通过 http://localhost:8080/ 访问。

## 生产输出

```shell
npm run build
```

![image](http://o6sbyl9mg.bkt.clouddn.com/d3in20days.jpg)

